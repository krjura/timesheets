#!/bin/bash

set -o xtrace

mkdir -p /opt/tmpfs/postgres-data
chmod a+rwx /opt/tmpfs/postgres-data
chown postgres:postgres /opt/tmpfs/postgres-data

/docker-entrypoint.sh "$@"
