package org.krjura.timesheets.service.repository

import org.krjura.timesheets.service.model.Organization
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository

interface OrganizationRepository : CrudRepository<Organization, Long> {

  @Query("select * from organization")
  override fun findAll(): List<Organization>

}