package org.krjura.timesheets.service.model

import org.krjura.timesheets.service.model.embedable.AuditTimestamps
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Embedded
import org.springframework.data.relational.core.mapping.Table

@Table("app_user")
data class AppUser(
  @Column("id") @Id val id: Long? = null,
  @Column("first_name") val firstName: String,
  @Column("last_name") val lastName: String,
  @Column("email") val email: String,
  @Embedded.Nullable val audit: AuditTimestamps
)