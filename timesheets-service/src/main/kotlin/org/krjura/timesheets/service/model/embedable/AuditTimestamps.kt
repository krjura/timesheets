package org.krjura.timesheets.service.model.embedable

import org.springframework.data.relational.core.mapping.Column
import java.time.LocalDateTime

data class AuditTimestamps(
  @Column("created_at") val createdAt: LocalDateTime,
  @Column("modified_at") val modifiedAt: LocalDateTime
)