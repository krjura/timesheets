package org.krjura.timesheets.service

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties
public open class TimesheetsParentApplication

fun main(args: Array<String>) {
  runApplication<TimesheetsParentApplication>(*args)
}
