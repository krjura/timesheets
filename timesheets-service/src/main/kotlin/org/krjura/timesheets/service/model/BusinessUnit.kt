package org.krjura.timesheets.service.model

import org.krjura.timesheets.service.model.embedable.AuditTimestamps
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Embedded
import org.springframework.data.relational.core.mapping.Table

@Table("business_unit")
public data class BusinessUnit(
  @Column("id") @Id val id: Long?,
  @Column("organization_id") val organizationId: Long,
  @Column("name") val name: String,
  @Embedded.Nullable val audit: AuditTimestamps
)