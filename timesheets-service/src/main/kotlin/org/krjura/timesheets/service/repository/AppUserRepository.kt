package org.krjura.timesheets.service.repository

import org.krjura.timesheets.service.model.AppUser
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository

interface AppUserRepository : CrudRepository<AppUser, Long> {

  @Query("select * from app_user")
  override fun findAll(): List<AppUser>
}