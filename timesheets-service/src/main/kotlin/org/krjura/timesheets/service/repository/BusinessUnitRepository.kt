package org.krjura.timesheets.service.repository

import org.krjura.timesheets.service.model.BusinessUnit
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository

interface BusinessUnitRepository : CrudRepository<BusinessUnit, Long> {

  @Query("select * from business_unit")
  override fun findAll(): List<BusinessUnit>
}