package org.krjura.timesheets.service.model

import org.krjura.timesheets.service.model.embedable.AuditTimestamps
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Embedded
import org.springframework.data.relational.core.mapping.Table

@Table("organization")
public data class Organization(
  @Column("id") @Id val id: Long?,
  @Column("name") val name: String,
  @Embedded.Nullable val audit: AuditTimestamps
)