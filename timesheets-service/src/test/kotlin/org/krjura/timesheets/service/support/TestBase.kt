package org.krjura.timesheets.service.support

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@ProjectTest
abstract class TestBase {

  @Autowired
  private lateinit var databaseCleaner: DatabaseCleaner;

  @BeforeEach
  fun beforeTestBase() {
    databaseCleaner.cleanAll()
  }

}