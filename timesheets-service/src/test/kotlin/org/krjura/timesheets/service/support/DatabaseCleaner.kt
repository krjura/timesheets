package org.krjura.timesheets.service.support

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component

@Component
class DatabaseCleaner(private val jdbcTemplate: JdbcTemplate) {

  fun cleanAll() {
    jdbcTemplate.update("TRUNCATE business_unit CASCADE");
    jdbcTemplate.update("TRUNCATE organization CASCADE");
    jdbcTemplate.update("TRUNCATE app_user CASCADE");
  }
}