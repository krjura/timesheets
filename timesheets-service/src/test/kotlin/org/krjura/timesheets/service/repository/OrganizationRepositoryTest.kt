package org.krjura.timesheets.service.repository

import org.krjura.timesheets.service.model.Organization
import org.krjura.timesheets.service.support.TestBase
import java.time.LocalDateTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.krjura.timesheets.service.model.embedable.AuditTimestamps
import org.springframework.beans.factory.annotation.Autowired

class OrganizationRepositoryTest : TestBase() {

  @Autowired
  private lateinit var repository: OrganizationRepository;

  @Test
  fun basicRepositoryTest() {
    repository.save(
      Organization(
        id = null,
        name = "TestOrg",
        audit = AuditTimestamps(
          createdAt = LocalDateTime.of(2019, 1, 1, 1, 1, 1),
          modifiedAt = LocalDateTime.of(2019, 1, 1, 1, 1, 2)
        )
      )
    )

    val organizations = repository.findAll();
    assertThat(organizations).hasSize(1)

    assertThat(organizations[0].name).isEqualTo("TestOrg")
    assertThat(organizations[0].audit.createdAt).isEqualTo(LocalDateTime.of(2019, 1, 1, 1, 1, 1))
    assertThat(organizations[0].audit.modifiedAt).isEqualTo(LocalDateTime.of(2019, 1, 1, 1, 1, 2))
  }
}