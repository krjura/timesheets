package org.krjura.timesheets.service.repository

import org.junit.jupiter.api.Test
import org.krjura.timesheets.service.model.AppUser
import org.krjura.timesheets.service.support.TestBase
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDateTime
import org.assertj.core.api.Assertions.assertThat;
import org.krjura.timesheets.service.model.embedable.AuditTimestamps

class AppUserRepositoryTest : TestBase() {

  @Autowired
  private lateinit var repository: AppUserRepository

  @Test
  fun basicRepositoryTest() {
    repository.save(AppUser(
      firstName = "Test",
      lastName = "User",
      email = "test@example.com",
      audit = AuditTimestamps(
        createdAt = LocalDateTime.of(2019, 1, 1, 1, 1, 1),
        modifiedAt = LocalDateTime.of(2019, 1, 1, 1, 1, 2)
      )
    ))

    val users = repository.findAll();
    assertThat(users).hasSize(1);

    assertThat(users[0].firstName).isEqualTo("Test")
    assertThat(users[0].lastName).isEqualTo("User")
    assertThat(users[0].email).isEqualTo("test@example.com")
    assertThat(users[0].audit.createdAt).isEqualTo(LocalDateTime.of(2019, 1, 1, 1, 1, 1))
    assertThat(users[0].audit.modifiedAt).isEqualTo(LocalDateTime.of(2019, 1, 1, 1, 1, 2))
  }
}