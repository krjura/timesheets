package org.krjura.timesheets.service.repository

import org.krjura.timesheets.service.model.Organization
import org.krjura.timesheets.service.support.TestBase
import java.time.LocalDateTime
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.krjura.timesheets.service.model.BusinessUnit
import org.krjura.timesheets.service.model.embedable.AuditTimestamps
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.relational.core.conversion.DbActionExecutionException

class BusinessUnitRepositoryTest : TestBase() {

  @Autowired
  private lateinit var organizationRepository: OrganizationRepository;

  @Autowired
  private lateinit var repository: BusinessUnitRepository;

  @Test
  fun basicRepositoryTest() {
    val organization = organizationRepository.save(
      Organization(
        id = null,
        name = "TestOrg",
        audit = AuditTimestamps(
          createdAt = LocalDateTime.of(2019, 1, 1, 1, 1, 1),
          modifiedAt = LocalDateTime.of(2019, 1, 1, 1, 1, 2)
        )
      )
    )

    repository.save(
      BusinessUnit(
        id = null,
        organizationId = organization.id!!,
        name = "TestBU",
        audit = AuditTimestamps(
          createdAt = LocalDateTime.of(2019, 1, 1, 1, 1, 1),
          modifiedAt = LocalDateTime.of(2019, 1, 1, 1, 1, 2)
        )
      )
    )

    val businessUnits = repository.findAll();
    assertThat(businessUnits).hasSize(1)

    assertThat(businessUnits[0].name).isEqualTo("TestBU")
    assertThat(businessUnits[0].organizationId).isEqualTo(organization.id!!)
    assertThat(businessUnits[0].audit.createdAt).isEqualTo(LocalDateTime.of(2019, 1, 1, 1, 1, 1))
    assertThat(businessUnits[0].audit.modifiedAt).isEqualTo(LocalDateTime.of(2019, 1, 1, 1, 1, 2))
  }

  @Test
  fun failIfOrganizationNotPresent() {

    val ex = assertThrows<DbActionExecutionException>("foreign key violation") {
      repository.save(
        BusinessUnit(
          id = null,
          organizationId = 1,
          name = "TestBU",
          audit = AuditTimestamps(
            createdAt = LocalDateTime.of(2019, 1, 1, 1, 1, 1),
            modifiedAt = LocalDateTime.of(2019, 1, 1, 1, 1, 2)
          )
        )
      )
    }

    assertThat(ex).hasCauseInstanceOf(DataIntegrityViolationException::class.java)
  }
}