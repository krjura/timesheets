#!/usr/bin/env bash

export PGPASSWORD=password
DIR=$(dirname $(readlink -f $0))

echo $DIR

psql -U postgres -h 127.0.0.1 -f $DIR/create-databases.sql