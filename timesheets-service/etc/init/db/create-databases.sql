DROP DATABASE IF EXISTS timesheets_service;
CREATE DATABASE timesheets_service ENCODING 'UTF-8';

DROP DATABASE IF EXISTS timesheets_service_tst;
CREATE DATABASE timesheets_service_tst ENCODING 'UTF-8';

DROP USER IF EXISTS timesheets_service;
CREATE USER timesheets_service WITH PASSWORD 'timesheets_service';

DROP USER IF EXISTS timesheets_service_tst;
CREATE USER timesheets_service_tst WITH PASSWORD 'timesheets_service_tst';