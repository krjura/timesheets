CREATE SEQUENCE seq_app_user_pk START WITH 1 INCREMENT BY 1 NO CYCLE ;

CREATE TABLE app_user (
  id INTEGER not null DEFAULT nextval('seq_app_user_pk'),
  first_name VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL,
  modified_at TIMESTAMP WITH TIME ZONE NOT NULL
);

ALTER TABLE app_user ADD CONSTRAINT pk_app_user PRIMARY KEY (id);