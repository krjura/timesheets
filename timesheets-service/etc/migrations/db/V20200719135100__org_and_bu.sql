CREATE SEQUENCE seq_organization_pk START WITH 1 INCREMENT BY 1 NO CYCLE ;

CREATE TABLE organization (
  id INTEGER not null DEFAULT nextval('seq_organization_pk'),
  name VARCHAR(255) NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL,
  modified_at TIMESTAMP WITH TIME ZONE NOT NULL
);

ALTER TABLE organization ADD CONSTRAINT pk_organization PRIMARY KEY (id);

CREATE SEQUENCE seq_business_unit_pk START WITH 1 INCREMENT BY 1 NO CYCLE ;

CREATE TABLE business_unit (
  id INTEGER not null DEFAULT nextval('seq_business_unit_pk'),
  organization_id INTEGER NOT NULL,
  name VARCHAR(255) NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE NOT NULL,
  modified_at TIMESTAMP WITH TIME ZONE NOT NULL
);

ALTER TABLE business_unit ADD CONSTRAINT pk_business_unit PRIMARY KEY (id);
ALTER TABLE business_unit ADD CONSTRAINT fk_business_unit_organization FOREIGN KEY (organization_id) REFERENCES organization (id);