package org.krjura.timesheets.timesheetsparent

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TimesheetsParentApplication

fun main(args: Array<String>) {
	runApplication<TimesheetsParentApplication>(*args)
}
